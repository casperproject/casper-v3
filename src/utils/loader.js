global.Modules = [];
global.Commands = [];

var getFiles = function(dir, filelist) {
  var path = path || require('path'); var fs = fs || require('fs'), files = fs.readdirSync(dir); filelist = filelist || [];
  files.forEach(function(file) { if (fs.statSync(path.join(dir, file)).isDirectory()) { filelist = getFiles(path.join(dir, file), filelist); } else { filelist.push(file); } });
  return filelist;
};

addCommands = ((list) => {
  list.forEach((command) => {
    Commands.push(command);
  })
});

this.loadModules = (() => {
  let path = __dirname.split('\\');
  let index = path.indexOf(path[path.length-1]); path.splice(index, 1);
  path = path.join('\\'); path = path+'\\app\\modules\\';

  let modules = getFiles(path);
  modules.forEach((lmod) => {
    try {
      let module = require(`../app/modules/${lmod}`);
      Modules.push(module);
      addCommands(module.commands);
      Logger.info('Loaded ' + lmod + ' fully.');
    } catch (err) { Logger.error('Failed to load ' + lmod + ' => ' + err.message); }
  });
});

this.loadHandlers = (() => {
  let path = __dirname.split('\\');
  let index = path.indexOf(path[path.length-1]); path.splice(index, 1);
  path = path.join('\\'); path = path+'\\app\\handlers\\';

  let handlers = getFiles(path);
  handlers.forEach((lhandle) => {
    let handle = require(`../app/handlers/${lhandle}`);
    Bot.on(handle.obj.type, (obj1, obj2, obj3, obj4) => {
      handle.obj.use(obj1, obj2, obj3, obj4);
    });
    Logger.info('Loaded handler ' + lhandle + ' fully.');
  });
});

this.reloadModules = (() => {
  let path = __dirname.split('\\');
  let index = path.indexOf(path[path.length-1]); path.splice(index, 1);
  path = path.join('\\'); path = path+'\\app\\modules\\';

  Modules = []
  Commands = []

  let modules = getFiles(path);
  modules.forEach((lmod) => {
    try {
      let lpath = '../app/modules/'+lmod
      delete require.cache[require.resolve(lpath)];
    } catch (err) { Logger.error('Failed to reload ' + lmod + ' => ' + err.message); }
  });
  this.loadModules();
});
