messageHandler = ((message) => {
  if(message.author.id === message.client.user.id) { return; }
  if(message.author.bot) { return; }
  let command = message.content.split(' ')[0].slice(Configuration.prefix.length);
  let params = message.content.split(' ').slice(1);

  Commands.forEach((parseCommand) => {
    try {
      if(parseCommand.names.indexOf(command) > -1) {
        parseCommand.use(message, params);
        if(Configuration.debugMode) {
          Logger.custom('command', `User ${message.author.username}(${message.author.id}) used command '${command}' in ${message.guild.name}(${message.guild.id})`);
        }
      }
    } catch(err) { Logger.error("Command " + parseCommand.names[0] + " generated error from " + parseCommand.module + " => " +  err.message); }
  });

});

this.obj = {
  type: 'message',
  use: messageHandler
}
