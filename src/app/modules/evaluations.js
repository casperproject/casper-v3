const moduleInfo = {
  name: 'Evaluations',
  truename: 'evaluations',
  author: 'Samuel (xCykrix)',
  contributors: []
}

runEval = (message, params) => {
  let command = "eval";
  if(!hasPermissions(message, message.author, 'dev')) { return; }
  let reply = ""; try { reply = eval(params.join(' ')); } catch(evalError) {
    message.channel.sendMessage("`Code Eval`\n" + '```js\n' + "Error: " + evalError.message + " ```" ).catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return;
  }
  message.channel.sendMessage('`Code Eval`\n' + '```js\n' + reply + " ```").catch((err) => {
    if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

var Math = require('mathjs');
runCalc = (message, params) => {
  let command = "calc";
  let reply = ""; try { reply = Math.eval(params.join(' ')); } catch(evalError) {
    message.channel.sendMessage("`Math Eval`\n" + '```js\n' + "Error: " + evalError.message + " ```" ).catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return;
  }
  message.channel.sendMessage('`Math Eval`\n' + '```js\n' + reply + " ```").catch((err) => {
    if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

this.commands = [
  { module: moduleInfo.truename, names: ['code', 'eval', 'evaluate'], use: runEval },
  { module: moduleInfo.truename, names: ['math', 'calc', 'calculate'], use: runCalc }
]
